Mocker
======

Installation
------------

    composer require griffin-mocker/griffin-mocker

Usage
-----

Provides access to infinite procedurally generated database. Querying the data results
in data generation making it appear as the whole database already exists.
One of the major benefits is that the database is of infinite size and can simulate
real life huge size databases.


Example Usage:

    <?php

    require __DIR__ . '/vendor/autoload.php';

    $mocker = new \GriffinMocker\Mocker('wklfj298dsjk21ypqmgz398j2lkdkeo2nd2');

    echo $mocker['table[id].field']->word() . "\n";


Example Model:

    <?php

    require __DIR__ . '/vendor/autoload.php';

    /**
     * Example mock repository.
     */
    class CityRepositoryMock {

      protected $mocker;

      function __construct ($mocker) {
        $this->mocker = $mocker;
      }

      function findOneById ($id) {
        return new CityEntityMock($this->mocker['city[' . $id . ']'], $id);
      }

    }

    /**
     * Example mock entity.
     */
    class CityEntityMock {

      protected $mocker;
      protected $id;

      function __construct ($mocker, $id) {
        $this->mocker = $mocker;
        $this->id = $id;
      }

      function getId () {
        return $this->id;
      }

      function getName () {
        return $this->mocker['name']->word();
      }

    }


    // Example usage:

    $cityRepository = new CityRepositoryMock(new \GriffinMocker\Mocker('wklfj298dsjk21ypqmgz398j2lkdkeo2nd2'));

    $city = $cityRepository->findOneById(1);

    echo $city->getId() . "\n";
    echo $city->getName() . "\n";
