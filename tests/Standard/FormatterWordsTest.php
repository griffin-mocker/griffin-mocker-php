<?php

class FormatterWordsTest extends \PHPUnit\Framework\TestCase {

  function testGeneral () {

    $mocker = new \GriffinMocker\Mocker('wklfj298dsjk21ypqmgz398j2lkdkeo2nd2');

    $this->assertEquals('dabihoa hyagimeze doi', $mocker['x']->words(3));
    $this->assertEquals('dabihoa', $mocker['x']->words([1, 3]));
    $this->assertEquals('dabihoa hyagimeze doi kyogua taryumau hemihagau', $mocker['x']->words([2, 8]));

  }

}
