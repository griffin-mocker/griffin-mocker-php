<?php

class FormatterShuffleTest extends \PHPUnit\Framework\TestCase {

  function testGeneral () {

    $mocker = new \GriffinMocker\Mocker('wklfj298dsjk21ypqmgz398j2lkdkeo2nd2');

    $this->assertEquals([], $mocker['x']->shuffle([]));
    $this->assertEquals(['a'], $mocker['x']->shuffle(['a']));

    $this->assertEquals(['c', 'b', 'a', 'd'], $mocker['a']->shuffle(['a', 'b', 'c', 'd']));
    $this->assertEquals(['b', 'c', 'd', 'a'], $mocker['b']->shuffle(['a', 'b', 'c', 'd']));
    $this->assertEquals(['b', 'd', 'a', 'c'], $mocker['c']->shuffle(['a', 'b', 'c', 'd']));
    $this->assertEquals(['d', 'c', 'a', 'b'], $mocker['d']->shuffle(['a', 'b', 'c', 'd']));
    $this->assertEquals(['d', 'b', 'a', 'c'], $mocker['e']->shuffle(['a', 'b', 'c', 'd']));
    $this->assertEquals(['d', 'c', 'a', 'b'], $mocker['f']->shuffle(['a', 'b', 'c', 'd']));
    $this->assertEquals(['c', 'b', 'd', 'a'], $mocker['g']->shuffle(['a', 'b', 'c', 'd']));
    $this->assertEquals(['b', 'c', 'a', 'd'], $mocker['h']->shuffle(['a', 'b', 'c', 'd']));
    $this->assertEquals(['a', 'c', 'd', 'b'], $mocker['i']->shuffle(['a', 'b', 'c', 'd']));

  }

}
