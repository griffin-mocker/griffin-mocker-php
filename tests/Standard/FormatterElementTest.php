<?php

class FormatterElementTest extends \PHPUnit\Framework\TestCase {

  /**
   * Test that arbitrary (non-numeric or non-ordered) keys produce expected result.
   */
  function testArbitraryKeys () {

    $mocker = new \GriffinMocker\Mocker('wklfj298dsjk21ypqmgz398j2lkdkeo2nd2');

    $this->assertEquals('a', $mocker['x']->element(['a' => 'a', 'b' => 'b']));

  }

}
