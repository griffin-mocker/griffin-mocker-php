<?php

class FormatterWordTest extends \PHPUnit\Framework\TestCase {

  function testGeneral () {

    $mocker = new \GriffinMocker\Mocker('wklfj298dsjk21ypqmgz398j2lkdkeo2nd2');

    $this->assertEquals('hie', $mocker['x']->word());
    $this->assertEquals('o', $mocker['x']->word(1, 1));
    $this->assertEquals('wo', $mocker['x']->word(2, 2));
    $this->assertEquals('hie', $mocker['x']->word(3, 3));
    $this->assertEquals('hiyu', $mocker['x']->word(4, 4));
    $this->assertEquals('hihya', $mocker['x']->word(5, 5));
    $this->assertEquals('hihyae', $mocker['x']->word(6, 6));
    $this->assertEquals('hihyaji', $mocker['x']->word(7, 7));
    $this->assertEquals('hihyagyo', $mocker['x']->word(8, 8));
    $this->assertEquals('hihyagyou', $mocker['x']->word(9, 9));
    $this->assertEquals('hihyagyome', $mocker['x']->word(10, 10));

  }

}
