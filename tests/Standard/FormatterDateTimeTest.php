<?php

class FormatterDateTimeTest extends \PHPUnit\Framework\TestCase {

  function testGeneral () {

    $mocker = new \GriffinMocker\Mocker('wklfj298dsjk21ypqmgz398j2lkdkeo2nd2');

    $this->assertEquals('1984-08-07T04:08:18+07:00', $mocker['x']->datetime()->format('c'));
    $this->assertEquals('1994-08-11T04:08:18+07:00', $mocker['x']->datetime(['1990-01-01', '2000-01-01'])->format('c'));
    $this->assertEquals('1985-07-28T04:08:18+07:00', $mocker['x']->datetime(['1983-01-01', '1986-01-01'])->format('c'));

  }

}
