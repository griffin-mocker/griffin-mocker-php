<?php

class FormatterRepeatTest extends \PHPUnit\Framework\TestCase {

  function testGeneral () {

    $mocker = new \GriffinMocker\Mocker('wklfj298dsjk21ypqmgz398j2lkdkeo2nd2');

    $this->assertEquals('dabihoa hyagimeze doi', $mocker['x']->repeat('word', 3, ' '));
    $this->assertEquals('dabihoa hyagimeze doi kyogua taryumau', $mocker['x']->repeat('word', [4, 7], ' '));

  }

}
