<?php

class FormatterWeightedElementTest extends \PHPUnit\Framework\TestCase {

  /**
   * Test that arbitrary (non-numeric or non-ordered) keys produce expected result.
   */
  function testArbitraryKeys () {

    $mocker = new \GriffinMocker\Mocker('wklfj298dsjk21ypqmgz398j2lkdkeo2nd2');

    $this->assertEquals('a', $mocker['a']->weightedElement(['a' => 1, 'b' => 1, 'c' => 3, 'd' => 9]));
    $this->assertEquals('d', $mocker['b']->weightedElement(['a' => 1, 'b' => 1, 'c' => 3, 'd' => 9]));
    $this->assertEquals('d', $mocker['c']->weightedElement(['a' => 1, 'b' => 1, 'c' => 3, 'd' => 9]));
    $this->assertEquals('c', $mocker['d']->weightedElement(['a' => 1, 'b' => 1, 'c' => 3, 'd' => 9]));
    $this->assertEquals('a', $mocker['e']->weightedElement(['a' => 1, 'b' => 1, 'c' => 3, 'd' => 9]));
    $this->assertEquals('d', $mocker['f']->weightedElement(['a' => 1, 'b' => 1, 'c' => 3, 'd' => 9]));
    $this->assertEquals('d', $mocker['g']->weightedElement(['a' => 1, 'b' => 1, 'c' => 3, 'd' => 9]));
    $this->assertEquals('d', $mocker['h']->weightedElement(['a' => 1, 'b' => 1, 'c' => 3, 'd' => 9]));
    $this->assertEquals('d', $mocker['i']->weightedElement(['a' => 1, 'b' => 1, 'c' => 3, 'd' => 9]));
    $this->assertEquals('d', $mocker['j']->weightedElement(['a' => 1, 'b' => 1, 'c' => 3, 'd' => 9]));
    $this->assertEquals('d', $mocker['k']->weightedElement(['a' => 1, 'b' => 1, 'c' => 3, 'd' => 9]));
    $this->assertEquals('d', $mocker['l']->weightedElement(['a' => 1, 'b' => 1, 'c' => 3, 'd' => 9]));
    $this->assertEquals('a', $mocker['m']->weightedElement(['a' => 1, 'b' => 1, 'c' => 3, 'd' => 9]));
    $this->assertEquals('d', $mocker['n']->weightedElement(['a' => 1, 'b' => 1, 'c' => 3, 'd' => 9]));
    $this->assertEquals('b', $mocker['o']->weightedElement(['a' => 1, 'b' => 1, 'c' => 3, 'd' => 9]));
    $this->assertEquals('d', $mocker['p']->weightedElement(['a' => 1, 'b' => 1, 'c' => 3, 'd' => 9]));
    $this->assertEquals('d', $mocker['r']->weightedElement(['a' => 1, 'b' => 1, 'c' => 3, 'd' => 9]));
    $this->assertEquals('c', $mocker['s']->weightedElement(['a' => 1, 'b' => 1, 'c' => 3, 'd' => 9]));
    $this->assertEquals('c', $mocker['t']->weightedElement(['a' => 1, 'b' => 1, 'c' => 3, 'd' => 9]));
    $this->assertEquals('d', $mocker['u']->weightedElement(['a' => 1, 'b' => 1, 'c' => 3, 'd' => 9]));
    $this->assertEquals('d', $mocker['v']->weightedElement(['a' => 1, 'b' => 1, 'c' => 3, 'd' => 9]));
    $this->assertEquals('c', $mocker['z']->weightedElement(['a' => 1, 'b' => 1, 'c' => 3, 'd' => 9]));

  }

  /**
   * Test that ordered index produce expected result.
   */
  function testOrderedIndex () {

    $mocker = new \GriffinMocker\Mocker('wklfj298dsjk21ypqmgz398j2lkdkeo2nd2');

    $this->assertEquals('a', $mocker['a']->weightedElement(['a' => 1, 'b' => 3, 'c' => 1, 'd' => 5], 0));
    $this->assertEquals('b', $mocker['b']->weightedElement(['a' => 1, 'b' => 3, 'c' => 1, 'd' => 5], 1));
    $this->assertEquals('c', $mocker['c']->weightedElement(['a' => 1, 'b' => 3, 'c' => 1, 'd' => 5], 2));
    $this->assertEquals('d', $mocker['d']->weightedElement(['a' => 1, 'b' => 3, 'c' => 1, 'd' => 5], 3));
    $this->assertEquals('b', $mocker['e']->weightedElement(['a' => 1, 'b' => 3, 'c' => 1, 'd' => 5], 4));
    $this->assertEquals('d', $mocker['f']->weightedElement(['a' => 1, 'b' => 3, 'c' => 1, 'd' => 5], 5));
    $this->assertEquals('b', $mocker['g']->weightedElement(['a' => 1, 'b' => 3, 'c' => 1, 'd' => 5], 6));
    $this->assertEquals('d', $mocker['h']->weightedElement(['a' => 1, 'b' => 3, 'c' => 1, 'd' => 5], 7));
    $this->assertEquals('d', $mocker['i']->weightedElement(['a' => 1, 'b' => 3, 'c' => 1, 'd' => 5], 8));
    $this->assertEquals('d', $mocker['j']->weightedElement(['a' => 1, 'b' => 3, 'c' => 1, 'd' => 5], 9));
    $this->assertEquals('a', $mocker['k']->weightedElement(['a' => 1, 'b' => 3, 'c' => 1, 'd' => 5], 10));
    $this->assertEquals('b', $mocker['l']->weightedElement(['a' => 1, 'b' => 3, 'c' => 1, 'd' => 5], 11));
    $this->assertEquals('c', $mocker['m']->weightedElement(['a' => 1, 'b' => 3, 'c' => 1, 'd' => 5], 12));
    $this->assertEquals('d', $mocker['n']->weightedElement(['a' => 1, 'b' => 3, 'c' => 1, 'd' => 5], 13));
    $this->assertEquals('b', $mocker['o']->weightedElement(['a' => 1, 'b' => 3, 'c' => 1, 'd' => 5], 14));
    $this->assertEquals('d', $mocker['p']->weightedElement(['a' => 1, 'b' => 3, 'c' => 1, 'd' => 5], 15));
    $this->assertEquals('b', $mocker['r']->weightedElement(['a' => 1, 'b' => 3, 'c' => 1, 'd' => 5], 16));
    $this->assertEquals('d', $mocker['s']->weightedElement(['a' => 1, 'b' => 3, 'c' => 1, 'd' => 5], 17));
    $this->assertEquals('d', $mocker['t']->weightedElement(['a' => 1, 'b' => 3, 'c' => 1, 'd' => 5], 18));
    $this->assertEquals('d', $mocker['u']->weightedElement(['a' => 1, 'b' => 3, 'c' => 1, 'd' => 5], 19));
    $this->assertEquals('a', $mocker['v']->weightedElement(['a' => 1, 'b' => 3, 'c' => 1, 'd' => 5], 20));
    $this->assertEquals('b', $mocker['z']->weightedElement(['a' => 1, 'b' => 3, 'c' => 1, 'd' => 5], 21));

  }

}
