<?php

class FormatterUniqueIDTest extends \PHPUnit\Framework\TestCase {

  function testGeneral () {

    $mocker = new \GriffinMocker\Mocker('wklfj298dsjk21ypqmgz398j2lkdkeo2nd2');

    $this->assertEquals('5wpoxzfszk8slpklnxpi8wukbuqk', $mocker['a']['b']['c'][1]->uniqueID());
    $this->assertEquals('kdc3wxjr10gezweyw2iaf9vrh3oc', $mocker['a']['b']['c'][2]->uniqueID());
    $this->assertEquals('wcz1vrmkc61djkmgfjbia4pmd6mt', $mocker['a']['b']['c'][3]->uniqueID());

  }
}
