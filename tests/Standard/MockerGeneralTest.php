<?php

class MockerGeneralTest extends \PHPUnit\Framework\TestCase {

  function testGeneral () {

    $mocker = new \GriffinMocker\Mocker('wklfj298dsjk21ypqmgz398j2lkdkeo2nd2');

    $this->assertEquals('zonie', $mocker['name']->word());
    $this->assertEquals(868, $mocker['height']->number());

    $this->assertEquals('myo', $mocker['city'][1]['name']->word());
    $this->assertEquals(16075644, $mocker['city'][1]['population']->number(100000000));
    $this->assertEquals(
      'Kyukyuji myazya chihao byamou. Kyubo kyuwai mewo hiza. Uhageto hyuryuyai.',
      $mocker['city'][1]['description']->text(10)
    );
  }
}
