<?php

namespace GriffinMocker;

class Mocker implements \ArrayAccess {

  /** @immutable @var string */
  protected $seed = '';

  function __construct ($seed) {

    $this->seed = $seed;

    $this->addFormatter(\GriffinMocker\Formatter\DateTime::class);
    $this->addFormatter(\GriffinMocker\Formatter\Element::class);
    $this->addFormatter(\GriffinMocker\Formatter\Number::class);
    $this->addFormatter(\GriffinMocker\Formatter\Repeat::class);
    $this->addFormatter(\GriffinMocker\Formatter\Shuffle::class);
    $this->addFormatter(\GriffinMocker\Formatter\Text::class);
    $this->addFormatter(\GriffinMocker\Formatter\UniqueID::class);
    $this->addFormatter(\GriffinMocker\Formatter\WeightedElement::class);
    $this->addFormatter(\GriffinMocker\Formatter\Word::class);
    $this->addFormatter(\GriffinMocker\Formatter\Words::class);

  }

  protected $formatters = [];

  function addFormatter ($formatter) {
    $this->formatters[] = $formatter;
  }

  function offset ($offset) {
    return new MockerEntry($this->seed, $this->formatters, $offset);
  }

  function offsetExists ($offset) {
    return true;
  }

  function offsetGet ($offset) {
    return $this->offset($offset);
  }

  function offsetSet ($offset, $value) {
    assert(false, 'Unable to set a read-only value.');
  }

  function offsetUnset ($offset) {
    assert(false, 'Unable to unset virtual value.');
  }

}
