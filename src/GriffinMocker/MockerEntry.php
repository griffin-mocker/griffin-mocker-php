<?php

namespace GriffinMocker;

use \ErrorException as Error;
use \Exception;

class MockerEntry implements \ArrayAccess {

  /** @immutable @var string */
  protected $seed = '';

  protected $formatters = [];

  /** @immutable @var string */
  protected $offset = '';

  function __construct ($seed, $formatters, $offset) {
    $this->seed = $seed;
    $this->formatters = $formatters;
    $this->offset = self::serializeOffset($offset);
  }

  static function serializeOffset ($offset) {
    $serializedOffset = $offset;
    if (is_array($offset)) {
      $serializedOffset = '';
      ksort($offset);
      foreach ($offset as $name => $value) {
        $serializedOffset .= $name . '=' . $value . ',';
      }
    }
    return $serializedOffset;
  }

  function offset ($offset) {
    return new MockerEntry($this->seed, $this->formatters, $this->offset . '.' . $offset);
  }

  function offsetExists ($offset) {
    return true;
  }

  function offsetGet ($offset) {
    return $this->offset($offset);
  }

  function offsetSet ($offset, $value) {
    assert(false, 'Unable to set a read-only value.');
  }

  function offsetUnset ($offset) {
    assert(false, 'Unable to unset virtual value.');
  }

  function __call ($name, $arguments) {
    foreach ($this->formatters as $formatter)
      if (method_exists($formatter, $name))
        return call_user_func_array([$formatter, $name], array_merge([$this], $arguments));
    throw new Exception('Griffin formatter `' . $name . '` is not defined.');
  }

  /**
   * Query raw bytes among procedurally generated data.
   *
   * @param integer $count How many bytes to return.
   */
  function bytes ($count = 20) {
    $output = '';
    for ($i = 0; strlen($output) < ceil($count); $i += 1)
      $output .= substr(hash('sha3-224', $this->seed . $this->offset . $i, true), 0, ceil($count) - strlen($output));
    return $output;
  }

}
