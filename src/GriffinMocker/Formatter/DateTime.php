<?php

namespace GriffinMocker\Formatter;

use \GriffinMocker\MockerEntry;

class DateTime {

  /**
   * Query DateTime among procedurally generated data.
   *
   * @param MockerEntry $mockerEntry
   * @param array|integer $wordCount Number of words to be generated.
   */
  static function datetime (MockerEntry $mockerEntry, $variance = [0, '2000-01-01']) {

    $from = $variance;

    if (is_array($from)) {
      $to = $from[1];
      $from = $from[0];
    }

    if (is_string($from))
      $from = (new \DateTime($from, new \DateTimeZone('UTC')))->getTimestamp();

    if (is_string($to))
      $to = (new \DateTime($to, new \DateTimeZone('UTC')))->getTimestamp();

    static $timezones = [];

    if (count($timezones) == 0) {
      $timezones = \DateTimeZone::listIdentifiers();
      sort($timezones, SORT_STRING);
    }

    $dateTime = new \DateTime();
    $dateTime->setTimestamp($mockerEntry->number([$from, $to]));
    $dateTime->setTimezone(new \DateTimeZone($mockerEntry['timezone']->element($timezones)));

    return $dateTime;

  }

}
