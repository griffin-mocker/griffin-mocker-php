<?php

namespace GriffinMocker\Formatter;

use \GriffinMocker\MockerEntry;

class WeightedElement {

  /**
   * Query an element from set among procedurally generated data.
   * Changes for a specific element to be returned are proportional to its weight.
   *
   * @param MockerEntry $mockerEntry
   * @param [$element => $weigth] $set Predefined set to pick from.
   */
  static function weightedElement (MockerEntry $mockerEntry, $set, $index = -1) {

    if ($index >= 0) {

      $index = $index % array_sum($set);

      $subset = array_filter($set, function ($elementWeight) use ($index, $set) {
        return floor($index / count($set)) < $elementWeight;
      });

      if (count($subset) < count($set))
        return self::weightedElement($mockerEntry, $subset, $index - (array_sum($set) - array_sum($subset)));

      return array_keys($set)[$index % count($set)];

    }

    $index = $mockerEntry->number(array_sum($set));

    $n = 0;
    foreach ($set as $elementName => $elementWeight) {
      if ($index < $n + $elementWeight)
        return $elementName;
      $n += $elementWeight;
    }

    assert(false);

  }

}
