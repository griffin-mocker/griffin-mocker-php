<?php

namespace GriffinMocker\Formatter;

use \GriffinMocker\MockerEntry;

class Element {

  /**
   * Query an element from set among procedurally generated data.
   *
   * @param MockerEntry $mockerEntry
   * @param mixed $set Predefined set to pick from.
   */
  static function element (MockerEntry $mockerEntry, $set) {
    return array_values($set)[$mockerEntry->number(count($set))];
  }

}
