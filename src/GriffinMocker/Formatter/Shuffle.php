<?php

namespace GriffinMocker\Formatter;

use \GriffinMocker\MockerEntry;

class Shuffle {

  /**
   * Shuffle a set based on seed provided to the mocker.
   *
   * @param MockerEntry $mockerEntry
   * @param mixed $set Predefined set to shuffle.
   */
  static function shuffle (MockerEntry $mockerEntry, $set) {
    return self::_permutation(array_values($set), $mockerEntry->number(self::_factorial(count($set))) + 1);
  }

  static function _permutation ($set, $permutation) {

    if (count($set) <= 1)
      return $set;

    $index = ceil($permutation / self::_factorial(count($set) - 1)) - 1;
    return array_merge([$set[$index]], self::_permutation(
      array_merge(array_slice($set, 0, $index), array_slice($set, $index + 1)),
      (($permutation - 1) % self::_factorial(count($set) - 1)) + 1
    ));

  }

  static function _factorial ($n) {
    return array_reduce(range(1, $n), function ($total, $carry) {
      return ($total == 0 ? 1 : $total) * $carry;
    });
  }

}
