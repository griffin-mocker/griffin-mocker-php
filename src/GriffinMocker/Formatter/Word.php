<?php

namespace GriffinMocker\Formatter;

use \GriffinMocker\MockerEntry;

class Word {

  /**
   * Query a word among procedurally generated data.
   *
   * @param MockerEntry $mockerEntry
   * @param array|integer $length Word length.
   */
  static function word (MockerEntry $mockerEntry, $length = [3, 10]) {

    /**
     * @see https://en.wikipedia.org/wiki/Hiragana#Table_of_hiragana
     */
    static $grammar = [
      'syllables' => [
        'a',
        'ba', 'be', 'bi', 'bo', 'bu', 'bya', 'byo', 'byu',
        'chi',
        'da', 'de', 'do',
        'e',
        'fu',
        'ga', 'ge', 'gi', 'go', 'gu', 'gya', 'gyo', 'gyu',
        'ha', 'he', 'hi', 'ho', 'hya', 'hyo', 'hyu',
        'i',
        'ji',
        'ka', 'ke', 'ki', 'ko', 'ku', 'kya', 'kyo', 'kyu',
        'ma', 'me', 'mi', 'mo', 'mu', 'mya', 'myo', 'myu',
        'n', 'na', 'ne', 'ni', 'no', 'nu', 'nya', 'nyo', 'nyu',
        'o',
        'pa', 'pe', 'pi', 'po', 'pu', 'pya', 'pyo', 'pyu',
        'ra', 're', 'ri', 'ro', 'ru', 'rya', 'ryo', 'ryu',
        'sa', 'se', 'shi', 'so', 'su', 'sya', 'syo', 'syu',
        'ta', 'te', 'to', 'tsu', 'tya', 'tyo', 'tyu',
        'u',
        'vu',
        'wa', 'we', 'wi', 'wo',
        'ya', 'yo', 'yu',
        'za', 'ze', 'zo', 'zu', 'zya', 'zyo', 'zyu',
      ],
      'rejectMap' => [
        'a-a',
        'ba-a', 'be-e', 'bi-i', 'bo-o', 'bu-u', 'bya-a', 'byo-o', 'byu-u',
        'chi-i',
        'da-a', 'de-e', 'do-o',
        'e-e',
        'fu-u',
        'ga-a', 'ge-e', 'gi-i', 'go-o', 'gu-u', 'gya-a', 'gyo-o', 'gyu-u',
        'ha-a', 'he-e', 'hi-i', 'ho-o', 'hya-a', 'hyo-o', 'hyu-u',
        'i-i',
        'ji-i',
        'ka-a', 'ke-e', 'ki-i', 'ko-o', 'ku-u', 'kya-a', 'kyo-o', 'kyu-u',
        'ma-a', 'me-e', 'mi-i', 'mo-o', 'mu-u', 'mya-a', 'myo-o', 'myu-u',
        'n-n', 'na-a', 'ne-e', 'ni-i', 'no-o', 'nu-u', 'nya-a', 'nyo-o', 'nyu-u',
        'o-o',
        'pa-a', 'pe-e', 'pi-i', 'po-o', 'pu-u', 'pya-a', 'pyo-o', 'pyu-u',
        'ra-a', 're-e', 'ri-i', 'ro-o', 'ru-u', 'rya-a', 'ryo-o', 'ryu-u',
        'sa-a', 'se-e', 'shi-i', 'so-o', 'su-u', 'sya-a', 'syo-o', 'syu-u',
        'ta-a', 'te-e', 'to-o', 'tsu-u', 'tya-a', 'tyo-o', 'tyu-u',
        'u-u',
        'vu-u',
        'wa-a', 'we-e', 'wi-i', 'wo-o',
        'ya-a', 'yo-o', 'yu-u',
        'za-a', 'ze-e', 'zo-o', 'zu-u', 'zya-a', 'zyo-o', 'zyu-u',
      ],
    ];

    static $map = [];

    if (count($map) == 0) {

      sort($grammar['syllables'], SORT_STRING);

      foreach ($grammar['syllables'] as $preceding) {
        if (!isset($map[trim(strtolower($preceding))]))
          $map[trim(strtolower($preceding))] = [];
        foreach ($grammar['syllables'] as $syllable)
          if (!in_array(trim(strtolower($syllable)), $map[trim(strtolower($preceding))]))
            $map[trim(strtolower($preceding))][] = trim(strtolower($syllable));
      }

      foreach ($grammar['rejectMap'] as $mapping) {
        list($preceding, $syllable) = explode('-', $mapping);
        $map[trim(strtolower($preceding))] = array_filter($map[trim(strtolower($preceding))],
            function ($mappedSyllable) use ($syllable) {
          return $mappedSyllable != trim(strtolower($syllable));
        });
      }

      foreach ($map as $index => $_)
        sort($map[$index], SORT_STRING);

    }

    if (is_array($length))
      $length = $mockerEntry->number($length);

    $syllables = [];
    while (strlen(implode('', $syllables)) < $length) {
      if (count($syllables) == 0) {
        $syllables[] = $mockerEntry['syllable-' . count($syllables)]->element(array_filter($grammar['syllables'],
            function ($syllable) use ($syllables, $length) {
          return strlen($syllable) <= $length - strlen(implode('', $syllables));
        }));
        continue;
      }
      if (!array_key_exists($syllables[count($syllables) - 1], $map)) {
        throw new \Error('Mapping for `' . $syllables[count($syllables) - 1] . '` not found.');
        break;
      }
      $syllables[] = $mockerEntry['syllable-' . count($syllables)]->element(array_filter(
            $map[$syllables[count($syllables) - 1]], function ($syllable) use ($syllables, $length) {
          return strlen($syllable) <= $length - strlen(implode('', $syllables));
        }));
    }

    return implode('', $syllables);
  }

}
