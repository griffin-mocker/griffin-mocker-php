<?php

namespace GriffinMocker\Formatter;

use \GriffinMocker\MockerEntry;

class Repeat {

  /**
   * Repeat an arbitrary query an arbitrary number of times among procedurally generated data.
   *
   * @param MockerEntry $mockerEntry
   * @param string $formatter Formatter to repeat.
   * @param array|integer $count Number of repetitions.
   * @param string $delimiter Delimiter used for concatenation.
   */
  static function repeat (MockerEntry $mockerEntry, $formatter, $count, $delimiter = '') {
    if (is_array($count))
      $count = $mockerEntry->number($count);
    return implode($delimiter, array_map(function ($index) use ($mockerEntry, $formatter) {
      return call_user_func([$mockerEntry['repeat-' . $index], $formatter]);
    }, range(0, $count - 1)));
  }

}
