<?php

namespace GriffinMocker\Formatter;

use \GriffinMocker\MockerEntry;

class Words {

  /**
   * Query an arbitrary number of words from procedurally generated data.
   *
   * @param MockerEntry $mockerEntry
   * @param array|integer $wordCount Number of words to be generated.
   */
  static function words (MockerEntry $mockerEntry, $wordCount = 10) {
    return $mockerEntry->repeat('word', $wordCount, ' ');
  }

}
