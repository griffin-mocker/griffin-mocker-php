<?php

namespace GriffinMocker\Formatter;

use \GriffinMocker\MockerEntry;

class Number {

  /**
   * Query a number among procedurally generated data.
   *
   * The number will always be between 0 and $variance - 1.
   *
   * @param MockerEntry $mockerEntry
   * @param array|integer $variance How many variances of looked up type should there be.
   */
  static function number (MockerEntry $mockerEntry, $variance = 1000) {
    $base = 0;
    if (is_array($variance)) {
      $base = $variance[0];
      $variance = $variance[1] - $variance[0];
    }
    if ($variance <= 0)
      return 0;
    return $base + hexdec(bin2hex($mockerEntry->bytes(ceil(log($variance, 256))))) % $variance;
  }

}
