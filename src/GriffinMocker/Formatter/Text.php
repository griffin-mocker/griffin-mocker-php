<?php

namespace GriffinMocker\Formatter;

use \GriffinMocker\MockerEntry;

class Text {

  /**
   * Query a text among procedurally generated data.
   *
   * @param MockerEntry $mockerEntry
   * @param array|integer $wordCount Number of words to be generated.
   */
  static function text (MockerEntry $mockerEntry, $wordCount = 200) {
    if (is_array($wordCount))
      $wordCount = $mockerEntry->number($wordCount);
    $words = [];
    $sentenceOffset = 0;
    $wordInSentenceOffset = 0;
    for ($i = 0; $i < $wordCount; $i++) {
      $totalwordsInSentence = $mockerEntry['sentence-' . $sentenceOffset]->number(12) + 4;
      if ($wordInSentenceOffset == $totalwordsInSentence) {
        $sentenceOffset++;
        $wordInSentenceOffset = 0;
        $words[] = '.';
      }
      $word = $mockerEntry['word-' . $i]->word();
      if ($wordInSentenceOffset == 0) {
        $word = ucfirst($word);
      }
      $words[] = $word;
      $wordInSentenceOffset++;
    }
    $words[] = '.';
    return str_replace(' .', '.', implode(' ', $words));
  }

}
