<?php

namespace GriffinMocker\Formatter;

use \GriffinMocker\MockerEntry;

class UniqueID {

  /**
   * Query an unique identifier among procedurally generated data.
   *
   * @param MockerEntry $mockerEntry
   */
  static function uniqueID (MockerEntry $mockerEntry) {
    $bits = 144;
    $bytes = ceil($bits / log(36, 2));
    return substr(strtolower(str_replace(['+', '/', '='], '', base64_encode($mockerEntry->bytes($bytes)))), 0, $bytes);
  }

}
